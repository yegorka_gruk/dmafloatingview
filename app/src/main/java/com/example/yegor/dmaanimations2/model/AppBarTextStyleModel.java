package com.example.yegor.dmaanimations2.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;

public class AppBarTextStyleModel implements Parcelable {
    public static final Creator<AppBarTextStyleModel> CREATOR = new Creator<AppBarTextStyleModel>() {
        @Override
        public AppBarTextStyleModel createFromParcel(Parcel source) {
            return new AppBarTextStyleModel(source);
        }

        @Override
        public AppBarTextStyleModel[] newArray(int size) {
            return new AppBarTextStyleModel[size];
        }
    };

    @ColorRes
    private int mColorId;
    private String mText;
    private int mGravity;

    public AppBarTextStyleModel(int colorId, String text, int gravity) {
        mColorId = colorId;
        mText = text;
        mGravity = gravity;
    }

    public AppBarTextStyleModel() {
    }

    protected AppBarTextStyleModel(Parcel in) {
        this.mColorId = in.readInt();
        this.mText = in.readString();
        this.mGravity = in.readInt();
    }

    @ColorRes
    public int getColorId() {
        return mColorId;
    }

    public String getText() {
        return mText;
    }

    public int getGravity() {
        return mGravity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mColorId);
        dest.writeString(this.mText);
        dest.writeInt(this.mGravity);
    }
}
