package com.example.yegor.dmaanimations2.model;

import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;

import com.example.yegor.dmaanimations2.R;

public enum FloatingWindowType {
    PRIVATE_CUSTOMER(R.color.floating_button_customer, ButtonsType.REMIND_ME, ButtonsType.CUSTOMER_PROFILE, ButtonsType.NOTE),
    BUSINESS_CUSTOMER(R.color.floating_button_business, ButtonsType.REMIND_ME, ButtonsType.BUSINESS_PROFILE, ButtonsType.NOTE),
    UNREGISTERED_CUSTOMER(R.color.floating_button_unregistered, ButtonsType.REMIND_ME, ButtonsType.SHARING, ButtonsType.NOTE),
    LAND_LINE_NUMBER(R.color.floating_button_light_white, ButtonsType.REMIND_ME, ButtonsType.REPORT_SPAM, ButtonsType.NOTE),
    SPAM_NUMBER(0, ButtonsType.REPORT_SPAM, ButtonsType.REPORT_UNSPAM),
    REMIND_IN(0, ButtonsType.REMIND_IN_10_MIN, ButtonsType.REMIND_IN_1_HOUR, ButtonsType.REMIND_IN_CUSTOM);

    FloatingWindowType(@ColorRes int colorRes, @NonNull ButtonsType... buttons) {
        mColorRes = colorRes;
        mButtons = buttons;
    }

    @ColorRes
    private int mColorRes;
    private ButtonsType[] mButtons;

    @ColorRes
    public int getColorRes() {
        return mColorRes;
    }

    public ButtonsType[] getButtons() {
        return mButtons;
    }

    public void setColorRes(int colorRes) {
        mColorRes = colorRes;
    }
}