package com.example.yegor.dmaanimations2.model;

import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.example.yegor.dmaanimations2.R;

public enum ButtonsType {
    //    root actions
    REMIND_ME(R.string.remind_me, R.drawable.ic_remind_in, 0, ButtonAction.RemindMe, false),
    CUSTOMER_PROFILE(R.string.customer_profile, R.drawable.ic_profile, 0, ButtonAction.CustomerProfile, true),
    NOTE(R.string.note, R.drawable.ic_note, 0, ButtonAction.Note, true),
    BUSINESS_PROFILE(R.string.business_profile, R.drawable.ic_profile, 0, ButtonAction.BusinessProfile, true),
    SHARING(R.string.sharing, R.drawable.ic_share_black_24dp, 0, ButtonAction.Sharing, true),
    REPORT_SPAM(R.string.report_spam, 0, R.color.floating_button_spam, ButtonAction.ReportSpam, true),
    REPORT_UNSPAM(R.string.report_unspam, 0, R.color.floating_button_unspam, ButtonAction.ReportUnSpam, true),
    //    nested actions
    REMIND_IN_10_MIN(R.string.remind_in_10_min, R.drawable.ic_remind_in, 0, ButtonAction.RemindMe, true, REMIND_ME),
    REMIND_IN_1_HOUR(R.string.remind_in_1_hour, R.drawable.ic_remind_in, 0, ButtonAction.RemindMe, true, REMIND_ME),
    REMIND_IN_CUSTOM(R.string.remind_custom, R.drawable.ic_remind_in, 0, ButtonAction.RemindMe, false, REMIND_ME),
    REMIND_IN_CUSTOM_SELECTED(0, 0, 0, ButtonAction.RemindMe, true, REMIND_IN_CUSTOM);

    ButtonsType(@StringRes int titleId, @DrawableRes int iconId, @ColorRes int backgroundColorId, ButtonAction action, boolean isTerminal) {
        this(titleId, iconId, backgroundColorId, action, isTerminal, null);
    }

    ButtonsType(@StringRes int titleId, @DrawableRes int iconId, @ColorRes int backgroundColorId, ButtonAction action, boolean isTerminal, @Nullable ButtonsType parentAction) {
        mTitleId = titleId;
        mIconId = iconId;
        mBackgroundColorId = backgroundColorId;
        mAction = action;
        mParentAction = parentAction;
        mIsTerminal = isTerminal;
    }

    @StringRes
    private int mTitleId;
    @DrawableRes
    private int mIconId;
    /**
     * if resource is 0, it would be taken from FloatingWindowType.mColorRes
     */
    @ColorRes
    private int mBackgroundColorId;
    private ButtonAction mAction;
    private boolean mIsTerminal;
    @Nullable
    private ButtonsType mParentAction;

    public int getTitleId() {
        return mTitleId;
    }

    public int getIconId() {
        return mIconId;
    }

    public int getBackgroundColorId() {
        return mBackgroundColorId;
    }

    public ButtonAction getAction() {
        return mAction;
    }

    public boolean isIsTerminal() {
        return mIsTerminal;
    }

    @Nullable
    public ButtonsType getParentAction() {
        return mParentAction;
    }
}