package com.example.yegor.dmaanimations2;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import static android.content.Context.MODE_PRIVATE;

public class PreferencesUtil {

    private static final String FLOATING_WIDGET_SAVED_POSITION = "FLOATING_WIDGET_SAVED_POSITION";

    private final SharedPreferences settings;

    public PreferencesUtil(final Context context, final String prefName) {
        settings = context.getSharedPreferences(prefName, MODE_PRIVATE);
    }

    protected void setString(String key, String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    protected String getString(String key, String defaultValue) {
        return settings.getString(key, defaultValue);
    }

    public void setFloatingWidgetSavedPosition(String position){
        setString(FLOATING_WIDGET_SAVED_POSITION, position);
    }

    @Nullable
    public String getFloatingWidgetSavedPosition(){
        return getString(FLOATING_WIDGET_SAVED_POSITION, null);
    }
}
