package com.example.yegor.dmaanimations2;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.ChangeBounds;
import android.support.transition.Fade;
import android.support.transition.Scene;
import android.support.transition.Transition;
import android.support.transition.TransitionListenerAdapter;
import android.support.transition.TransitionManager;
import android.support.transition.TransitionSet;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.example.yegor.dmaanimations2.animation.ChangeShapeTransition;
import com.example.yegor.dmaanimations2.animation.RevealTransition;
import com.example.yegor.dmaanimations2.animation.TextSizeTransition;
import com.example.yegor.dmaanimations2.model.AppBarTextModel;
import com.example.yegor.dmaanimations2.model.AppBarTextStyleModel;
import com.example.yegor.dmaanimations2.model.ButtonsType;
import com.example.yegor.dmaanimations2.model.FloatingWindowType;
import com.example.yegor.dmaanimations2.model.WheelPickerItemModel;
import com.example.yegor.dmaanimations2.model.WindowCoordinates;
import com.example.yegor.dmaanimations2.utils.Utils;
import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;

import java.util.ArrayList;
import java.util.List;

public class FloatingWidgetService extends Service implements View.OnClickListener {

    public static final String BUTTON_CLICK_EVENT = "BUTTON_CLICK_EVENT";
    public static final String BUTTONS_HOLDER_TYPE_EXTRA = "BUTTONS_HOLDER_TYPE_EXTRA";
    public static final String ADDITIONAL_PARAMETER_EXTRA = "ADDITIONAL_PARAMETER_EXTRA";

    public static final String FLOATING_BUTTON_TYPE_EXTRA = "FLOATING_BUTTON_TYPE_EXTRA";
    public static final String TOOLBAR_TITLE_EXTRA = "TOOLBAR_TITLE_EXTRA";
    public static final String WINDOW_COORDINATES_EXTRA = "WINDOW_COORDINATES_EXTRA";

    private static final String YOUR_PLACEMENT_ID = "295351144372039_295355034371650";
    private static final int REVEAL_DURATION = 500;
    private static final int TRANSACTION_DURATION = 250;

    private WindowManager mWindowManager;
    private ViewGroup mFloatingWidgetView;
    private ViewGroup mCollapsedView;
    private ViewGroup mExpandedView;
    private ViewGroup mTimePickerLayout;
    private Point szWindow = new Point();

    @Nullable
    private AdView mAdView;
    @Nullable
    private AdView mTimePickerAdView;
    private boolean isCollapsed = true;
    private AppBarTextModel mAppBarTitleModel;
    private WindowCoordinates mWindowPosition;
    private FloatingWindowType mFloatingWindowType;
    private Scene mCollapsedScene;
    private Scene mExpandedScene;

    private int mExpandedViewWidth;
    private int mExpandedViewHeight;

    @Deprecated
    private WheelPicker mNumberPicker;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.getDefaultDisplay().getSize(szWindow);
        mAppBarTitleModel = intent.getParcelableExtra(TOOLBAR_TITLE_EXTRA);
        mWindowPosition = intent.getParcelableExtra(WINDOW_COORDINATES_EXTRA);
        mFloatingWindowType = (FloatingWindowType) intent.getSerializableExtra(FLOATING_BUTTON_TYPE_EXTRA);
        AdSettings.addTestDevice("ad9c0dd3-c0e6-4d1e-a8bc-3c2ba3ebd7c1");
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        addFloatingWidgetView(inflater);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        saveFloatingViewCoordinates();
        if (mFloatingWidgetView != null) {
            mWindowManager.removeView(mFloatingWidgetView);
        }
        if (mAdView != null) {
            mAdView.destroy();
        }
        if (mTimePickerAdView != null) {
            mTimePickerAdView.destroy();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mWindowManager.getDefaultDisplay().getSize(szWindow);
        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mFloatingWidgetView.getLayoutParams();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (layoutParams.y + (mFloatingWidgetView.getHeight() + getStatusBarHeight()) > szWindow.y) {
                layoutParams.y = szWindow.y - (mFloatingWidgetView.getHeight() + getStatusBarHeight());
                mWindowManager.updateViewLayout(mFloatingWidgetView, layoutParams);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_container:
            case R.id.close:
                stopSelf();
                break;
            case R.id.scene_root2:
                TransitionSet contentSet = new TransitionSet()
                        .addTransition(new ChangeBounds())
                        .addTransition(new ChangeShapeTransition(R.id.toolbar_container, getResources().getDimension(R.dimen.dimen_large)))
                        .addTransition(new TextSizeTransition())
                        .setOrdering(TransitionSet.ORDERING_TOGETHER)
                        .setDuration(TRANSACTION_DURATION);

                TransitionSet mainSet = new TransitionSet()
                        .addTransition(new Fade(Fade.OUT))
                        .addTransition(contentSet)
                        .addTransition(new Fade(Fade.IN))
                        .setOrdering(TransitionSet.ORDERING_SEQUENTIAL)
                        .setDuration(TRANSACTION_DURATION)
                        .setInterpolator(new AccelerateDecelerateInterpolator())
                        .addListener(new TransitionListenerAdapter() {
                            @Override
                            public void onTransitionEnd(@NonNull Transition transition) {
                                isCollapsed = !isCollapsed;
                                if (!isCollapsed) {
                                    mAdView = new AdView(FloatingWidgetService.this, YOUR_PLACEMENT_ID, AdSize.BANNER_HEIGHT_50);
                                    mAdView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                                    ((ViewGroup) Utils.findViewById(mExpandedView, R.id.banner_container)).addView(mAdView);
                                    mAdView.loadAd();
                                } else if (mAdView != null) {
                                    ((ViewGroup) Utils.findViewById(mExpandedView, R.id.banner_container)).removeView(mAdView);
                                    mAdView.destroy();
                                }
                                WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mFloatingWidgetView.getLayoutParams();
                                mWindowPosition.setX(layoutParams.x);
                                mWindowPosition.setY(layoutParams.y);
                                mWindowManager.updateViewLayout(mFloatingWidgetView, getLayoutParams());
                            }
                        });

                WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mFloatingWidgetView.getLayoutParams();
                mWindowPosition.setX(layoutParams.x);
                mWindowPosition.setY(layoutParams.y);
                mWindowManager.updateViewLayout(mFloatingWidgetView, getLayoutParams(mExpandedViewWidth, mExpandedViewHeight));

                if (isCollapsed) {
                    TransitionManager.go(mExpandedScene, mainSet);
                } else {
                    TransitionManager.go(mCollapsedScene, mainSet);
                }
                break;
            case R.id.button_1:
            case R.id.button_2:
            case R.id.button_3:
            case R.id.overflow_confirm:
                clickDelegate(v);
                break;
        }
    }

    private void saveFloatingViewCoordinates() {
        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) mFloatingWidgetView.getLayoutParams();
        mWindowPosition.setX(layoutParams.x);
        mWindowPosition.setY(layoutParams.y);
        App.getApp().getPreferences().setFloatingWidgetSavedPosition(mWindowPosition.toString());
    }

    private void clickDelegate(View view) {
        ButtonsType buttonsType = (ButtonsType) view.getTag(R.id.action_tag);
        if (buttonsType == null) {
            throw new IllegalStateException();
        }
        int startX;
        int startY;
        int endRadius;
        TransitionSet set;
        switch (buttonsType) {
            case REMIND_ME:
                startX = (view.getLeft() + view.getRight()) / 2;
                startY = (view.getTop() + view.getBottom()) / 2;

                endRadius = (int) Math.hypot(mExpandedView.getWidth(), mExpandedView.getHeight());

                ViewGroup buttonsContainer = Utils.findViewById(mExpandedView, R.id.buttons_container);
                buttonsContainer.removeAllViews();
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                ViewGroup newView = (ViewGroup) inflater.inflate(R.layout.item_expanded_main_three_buttons_container, null);

                FloatingWindowType remindIn = FloatingWindowType.REMIND_IN;
                remindIn.setColorRes(mFloatingWindowType.getColorRes());
                setUpButtons(newView, remindIn);

                final Scene scene1 = new Scene(buttonsContainer, newView);

                set = new TransitionSet();
                set.addTransition(new RevealTransition(startX, startY, 0, endRadius, R.id.buttons_container, false));
                set.setDuration(REVEAL_DURATION);

                TransitionManager.go(scene1, set);
                break;
            case REMIND_IN_CUSTOM:
                RelativeLayout.LayoutParams overflowViewParams = new RelativeLayout.LayoutParams(mExpandedView.getMeasuredWidth(), mExpandedView.getMeasuredHeight());
                mTimePickerLayout.setLayoutParams(overflowViewParams);

                View confirmButton = Utils.findViewById(mTimePickerLayout, R.id.overflow_confirm);
                confirmButton.setOnClickListener(this);
                confirmButton.setTag(R.id.action_tag, ButtonsType.REMIND_IN_CUSTOM_SELECTED);
                String[] values = getResources().getStringArray(R.array.remind_in_minutes_array);
                List<WheelPickerItemModel> itemModels = new ArrayList<>(values.length);
                for (String value : values) {
                    itemModels.add(WheelPickerItemModel.create(value));
                }
                mNumberPicker = Utils.findViewById(mTimePickerLayout, R.id.overflow_time_picker);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    Utils.tintDrawable(Utils.findViewById(mTimePickerLayout, R.id.container).getBackground(), getResources().getColor(R.color.appbar_color));
                    Utils.tintDrawable(mNumberPicker.getBackground(), getResources().getColor(R.color.avatar_tint));
                }
                mNumberPicker.setData(itemModels);

                if (mAdView != null) {
                    ImageView imageView = Utils.findViewById(mTimePickerLayout, R.id.overflow_banner_image_placeholder);
                    long nanoTime = System.nanoTime();
                    Bitmap drawingCache = Utils.getDrawingCacheCopy(mAdView);
                    long executionTime = System.nanoTime() - nanoTime;
                    Log.w("ExecutionTime", "0) banner.setBackground execution time " + executionTime + "ns(" + (executionTime / 1_000_000) + "ms)");
                    if (drawingCache != null) {
                        imageView.setImageBitmap(drawingCache);
                    }
                }

                final Scene scene2 = new Scene(mFloatingWidgetView, mTimePickerLayout);

                View parent = (View) view.getParent().getParent();
                startX = (view.getLeft() + view.getRight()) / 2;
                startY = parent.getTop() + (view.getTop() + view.getBottom()) / 2;
                endRadius = (int) Math.hypot(mExpandedView.getWidth(), mExpandedView.getHeight());

                set = new TransitionSet();
                set.addTransition(new RevealTransition(startX, startY, 0, endRadius, R.id.container, true));
                set.setDuration(REVEAL_DURATION);
                set.addListener(new TransitionListenerAdapter() {
                    @Override
                    public void onTransitionStart(@NonNull Transition transition) {
                        mTimePickerLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onTransitionEnd(@NonNull Transition transition) {
                        mTimePickerAdView = new AdView(FloatingWidgetService.this, YOUR_PLACEMENT_ID, AdSize.BANNER_HEIGHT_50);
                        mTimePickerAdView.loadAd();
                        mTimePickerAdView.setAdListener(new AbstractAdListener() {
                            @Override
                            public void onAdLoaded(Ad ad) {
                                ViewGroup bannerContainer = Utils.findViewById(mTimePickerLayout, R.id.overflow_banner_container);
                                bannerContainer.addView(mTimePickerAdView);
                                bannerContainer.removeViewAt(0);
                            }
                        });
                    }
                });

                TransitionManager.go(scene2, set);
                break;
            case REMIND_IN_CUSTOM_SELECTED:
                WheelPickerItemModel itemModel = (WheelPickerItemModel) mNumberPicker.getData().get(mNumberPicker.getCurrentItemPosition());
                sendMessage(buttonsType, "Title = " + itemModel.getTitle() + ", Minutes = " + itemModel.getMinutes());
                break;
            default:
                sendMessage(buttonsType, null);
        }
        if (buttonsType.isIsTerminal()) {
            stopSelf();
        }
    }

    @NonNull
    private WindowManager.LayoutParams getLayoutParams() {
        return getLayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private WindowManager.LayoutParams getLayoutParams(final int width, final int height) {
        int windowType;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            windowType = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            windowType = WindowManager.LayoutParams.TYPE_PHONE;
        }
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width, height, windowType,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.START;
        params.x = mWindowPosition.getX();
        params.y = mWindowPosition.getY();
        return params;
    }

    private void addFloatingWidgetView(LayoutInflater inflater) {
        mFloatingWidgetView = (ViewGroup) inflater.inflate(R.layout.main_window_widget_layout, null);
        mWindowManager.addView(mFloatingWidgetView, getLayoutParams());
        addSceneViews(inflater);
        mCollapsedScene = new Scene(mFloatingWidgetView, mCollapsedView);
        mExpandedScene = new Scene(mFloatingWidgetView, mExpandedView);
        mFloatingWidgetView.setOnTouchListener(new FloatingViewTouchListener(mWindowManager));
        mFloatingWidgetView.setOnClickListener(this);
    }

    private void addSceneViews(LayoutInflater inflater) {
        mCollapsedView = (ViewGroup) inflater.inflate(R.layout.collapsed_main_window_widget_layout, null);
        mExpandedView = (ViewGroup) inflater.inflate(R.layout.expanded_main_window_widget_layout, null);
        mTimePickerLayout = (ViewGroup) View.inflate(this, R.layout.overflow_time_picker, null);

//        precahce
        Utils.findViewById(mExpandedView, R.id.toolbar_container);
        Utils.findViewById(mExpandedView, R.id.bottom_part_background);
        Utils.findViewById(mExpandedView, R.id.buttons_container);
        Utils.findViewById(mExpandedView, R.id.appbar_text_container);
        Utils.findViewById(mExpandedView, R.id.banner_container);

        ViewGroup buttonsContainer = Utils.findViewById(mExpandedView, R.id.buttons_container);
        int layoutResource = mFloatingWindowType.getButtons().length == 3 ? R.layout.item_expanded_main_three_buttons_container : R.layout.item_expanded_main_two_buttons_container;
        inflater.inflate(layoutResource, buttonsContainer);

        setUpButtons((ViewGroup) buttonsContainer.getChildAt(0), mFloatingWindowType);

        ((TextView) Utils.findViewById(mCollapsedView, R.id.subtitle)).setText(mAppBarTitleModel.getCollapsedTitleText());
        ViewGroup appbarTextContainer = Utils.findViewById(mExpandedView, R.id.appbar_text_container);
        if (mAppBarTitleModel.getExpandedEndColorId() == 0) {
            appbarTextContainer.setBackgroundResource(mAppBarTitleModel.getExpandedStartColorId());
        } else {
            int startColor = getResources().getColor(mAppBarTitleModel.getExpandedStartColorId());
            int endColor = getResources().getColor(mAppBarTitleModel.getExpandedEndColorId());
            int[] colors = {startColor, startColor, endColor, endColor};
            appbarTextContainer.setBackground(new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors));
        }

        TextView textView;
        AppBarTextStyleModel[] expandedTitleText = mAppBarTitleModel.getExpandedTitleText();
        for (AppBarTextStyleModel appBarTextStyleModel : expandedTitleText) {
            textView = new TextView(this);
            textView.setText(appBarTextStyleModel.getText());
            textView.setTextColor(getResources().getColor(appBarTextStyleModel.getColorId()));
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, appBarTextStyleModel.getGravity());
            textView.setLayoutParams(params);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger(R.integer.appbar_title_text_size));
            textView.setTypeface(ResourcesCompat.getFont(this, R.font.roboto_font_medium));
            appbarTextContainer.addView(textView);
        }

        int tintColor = ContextCompat.getColor(this, R.color.appbar_color);
        View collapsedViewToolbarContainer = Utils.findViewById(mCollapsedView, R.id.toolbar_container);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Utils.tintDrawable(collapsedViewToolbarContainer.getBackground(), tintColor);
        }

        View expandedViewToolbarContainer = Utils.findViewById(mExpandedView, R.id.toolbar_container);
        Utils.tintDrawable(expandedViewToolbarContainer.getBackground(), tintColor);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            Utils.tintDrawable(Utils.findViewById(mExpandedView, R.id.bottom_part_background).getBackground(), getResources().getColor(R.color.grey_background));
        }

        Utils.findViewById(mCollapsedView, R.id.close_container).setOnClickListener(this);
        Utils.findViewById(mExpandedView, R.id.close_container).setOnClickListener(this);
        Utils.findViewById(mTimePickerLayout, R.id.close).setOnClickListener(this);
        mFloatingWidgetView.addView(mCollapsedView);
        measureExpandedView();
    }

    private void measureExpandedView() {
        mFloatingWidgetView.addView(mExpandedView);
        ViewTreeObserver viewTreeObserver = mExpandedView.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mExpandedView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    mExpandedViewWidth = mExpandedView.getWidth();
                    mExpandedViewHeight = mExpandedView.getHeight();
                    mFloatingWidgetView.removeView(mExpandedView);
                }
            });
        }
    }

    private void setUpButtons(ViewGroup buttonsContainer, FloatingWindowType windowType) {
        for (int i = 0; i < buttonsContainer.getChildCount(); i++) {
            ButtonsType buttonsType = windowType.getButtons()[i];
            View buttonRoot = buttonsContainer.getChildAt(i);
            if (buttonRoot instanceof ViewGroup) {
                ViewGroup buttonRootViewGroup = (ViewGroup) buttonRoot;
                int backgroundColorId = buttonsType.getBackgroundColorId();
                Utils.tintDrawable(buttonRoot.getBackground(), getResources().getColor(backgroundColorId != 0 ? backgroundColorId : windowType.getColorRes()));
                for (int j = 0; j < buttonRootViewGroup.getChildCount(); j++) {
                    View buttonRootViewGroupChild = buttonRootViewGroup.getChildAt(j);
                    if (buttonRootViewGroupChild instanceof ImageView) {
                        ImageView icon = (ImageView) buttonRootViewGroupChild;
                        icon.setImageResource(buttonsType.getIconId());
                    } else if (buttonRootViewGroupChild instanceof TextView) {
                        TextView text = (TextView) buttonRootViewGroupChild;
                        text.setText(buttonsType.getTitleId());
                    } else {
                        //this should no happens
                        throw new IllegalStateException();
                    }
                }
            } else {//it should be a textview
                int backgroundColorId = buttonsType.getBackgroundColorId();
                Utils.tintDrawable(buttonRoot.getBackground(), getResources().getColor(backgroundColorId != 0 ? backgroundColorId : windowType.getColorRes()));
                ((TextView) buttonRoot).setText(buttonsType.getTitleId());
            }
            buttonRoot.setTag(R.id.action_tag, buttonsType);
            buttonRoot.setOnClickListener(this);
        }
    }

    private int getStatusBarHeight() {
        return (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density);
    }

    private void sendMessage(ButtonsType buttonsType, String extra) {
        Intent intent = new Intent(BUTTON_CLICK_EVENT);
        intent.putExtra(BUTTONS_HOLDER_TYPE_EXTRA, buttonsType);
        intent.putExtra(ADDITIONAL_PARAMETER_EXTRA, extra);
        sendBroadcast(intent);
    }
}
