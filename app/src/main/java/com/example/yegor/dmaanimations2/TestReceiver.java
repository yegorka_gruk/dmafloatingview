package com.example.yegor.dmaanimations2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class TestReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TestReceiver", "onReceive() called with: " + intent.getSerializableExtra(FloatingWidgetService.BUTTONS_HOLDER_TYPE_EXTRA) + " | " + intent.getSerializableExtra(FloatingWidgetService.ADDITIONAL_PARAMETER_EXTRA));
        Toast.makeText(context, intent.getSerializableExtra(FloatingWidgetService.BUTTONS_HOLDER_TYPE_EXTRA) + " | " + intent.getSerializableExtra(FloatingWidgetService.ADDITIONAL_PARAMETER_EXTRA), Toast.LENGTH_SHORT).show();
    }
}
