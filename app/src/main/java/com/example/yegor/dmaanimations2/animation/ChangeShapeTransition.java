package com.example.yegor.dmaanimations2.animation;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.Transition;
import android.support.transition.TransitionValues;
import android.view.ViewGroup;

import java.lang.reflect.Field;

public class ChangeShapeTransition extends Transition {

    private static final String TAG = ChangeShapeTransition.class.getName();
    private static final String GRADIENT_STATE_FIELD = "mGradientState";
    private static final String RADIUS_ARRAY_FIELD = "mRadiusArray";
    private static final String PROPNAME_CORNER_RADII = "custom:background:corner_radii";

    @IdRes
    private int mContainerId;
    private float mStartDimension;

    public ChangeShapeTransition(@IdRes int containerId, float startDimension) {
        mContainerId = containerId;
        mStartDimension = startDimension;
    }

    @Override
    public void captureStartValues(@NonNull TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    @Override
    public void captureEndValues(@NonNull TransitionValues transitionValues) {
        captureValues(transitionValues);
    }

    @Nullable
    @Override
    public Animator createAnimator(@NonNull ViewGroup sceneRoot, @Nullable TransitionValues startValues, @Nullable TransitionValues endValues) {
        if (startValues != null && endValues != null && mContainerId == endValues.view.getId()) {
            GradientDrawable drawable = (GradientDrawable) endValues.view.getBackground();
            float[] startRadii = (float[]) startValues.values.get(PROPNAME_CORNER_RADII);
            float[] endRadii = (float[]) endValues.values.get(PROPNAME_CORNER_RADII);
            float[] cornerArray = new float[8];
            float startRadius = startRadii[0];
            ValueAnimator animator = ValueAnimator.ofFloat(startRadius, endRadii[0]);
            animator.setDuration(getDuration());
            animator.addUpdateListener((animation) -> {
                float topValue = (float) animation.getAnimatedValue();
                float bottomValue = topValue * 2 - mStartDimension;
                cornerArray[0] = topValue;
                cornerArray[1] = topValue;
                cornerArray[2] = topValue;
                cornerArray[3] = topValue;
                cornerArray[4] = bottomValue;
                cornerArray[5] = bottomValue;
                cornerArray[6] = bottomValue;
                cornerArray[7] = bottomValue;
                drawable.setCornerRadii(cornerArray);
            });

            float bottomValue = startRadius * 2 - mStartDimension;
            cornerArray[0] = startRadius;
            cornerArray[1] = startRadius;
            cornerArray[2] = startRadius;
            cornerArray[3] = startRadius;
            cornerArray[4] = bottomValue;
            cornerArray[5] = bottomValue;
            cornerArray[6] = bottomValue;
            cornerArray[7] = bottomValue;
            drawable.setCornerRadii(cornerArray);
            return animator;
        }
        return null;
    }

    private void captureValues(TransitionValues transitionValues) {
        if (mContainerId == transitionValues.view.getId()) {
            GradientDrawable background = (GradientDrawable) transitionValues.view.getBackground();
            float[] cornerRadii;
            try {
                cornerRadii = getCornerRadii(background);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new IllegalStateException();
            }
            transitionValues.values.put(PROPNAME_CORNER_RADII, cornerRadii);
        }
    }

    private float[] getCornerRadii(GradientDrawable background) throws NoSuchFieldException, IllegalAccessException {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return background.getCornerRadii();
        } else {
            Field f = background.getClass().getDeclaredField(GRADIENT_STATE_FIELD);
            f.setAccessible(true);
            Object o = f.get(background);
            f = o.getClass().getDeclaredField(RADIUS_ARRAY_FIELD);
            f.setAccessible(true);
            return (float[]) f.get(o);
        }
    }
}
