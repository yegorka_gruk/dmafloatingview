package com.example.yegor.dmaanimations2.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;
import android.support.annotation.Size;

import java.util.Arrays;

public class AppBarTextModel implements Parcelable {
    public static final Parcelable.Creator<AppBarTextModel> CREATOR = new Parcelable.Creator<AppBarTextModel>() {
        @Override
        public AppBarTextModel createFromParcel(Parcel source) {
            return new AppBarTextModel(source);
        }

        @Override
        public AppBarTextModel[] newArray(int size) {
            return new AppBarTextModel[size];
        }
    };

    private String mCollapsedTitleText;
    @ColorRes
    private int mExpandedStartColorId;
    @ColorRes
    private int mExpandedEndColorId;
    private AppBarTextStyleModel[] mExpandedTitleText;

    public AppBarTextModel() {
    }

    public AppBarTextModel(String collapsedTitleText, int expandedStartColorId, @Size(min = 1, max = 2) AppBarTextStyleModel[] expandedTitleText) {
        this(collapsedTitleText, expandedStartColorId, 0, expandedTitleText);
    }

    public AppBarTextModel(String collapsedTitleText, int expandedStartColorId, int expandedEndColorId, @Size(min = 1, max = 2) AppBarTextStyleModel[] expandedTitleText) {
        mCollapsedTitleText = collapsedTitleText;
        mExpandedStartColorId = expandedStartColorId;
        mExpandedEndColorId = expandedEndColorId;
        mExpandedTitleText = expandedTitleText;
    }

    protected AppBarTextModel(Parcel in) {
        this.mCollapsedTitleText = in.readString();
        this.mExpandedStartColorId = in.readInt();
        this.mExpandedEndColorId = in.readInt();
        this.mExpandedTitleText = in.createTypedArray(AppBarTextStyleModel.CREATOR);
    }

    public String getCollapsedTitleText() {
        return mCollapsedTitleText;
    }

    @ColorRes
    public int getExpandedStartColorId() {
        return mExpandedStartColorId;
    }

    @ColorRes
    public int getExpandedEndColorId() {
        return mExpandedEndColorId;
    }

    public AppBarTextStyleModel[] getExpandedTitleText() {
        return mExpandedTitleText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mCollapsedTitleText);
        dest.writeInt(this.mExpandedStartColorId);
        dest.writeInt(this.mExpandedEndColorId);
        dest.writeTypedArray(this.mExpandedTitleText, flags);
    }

    @Override
    public String toString() {
        return "AppBarTextModel{" +
                "mCollapsedTitleText='" + mCollapsedTitleText + '\'' +
                ", mExpandedStartColorId=" + mExpandedStartColorId +
                ", mExpandedColorId=" + mExpandedEndColorId +
                ", mExpandedTitleText=" + Arrays.toString(mExpandedTitleText) +
                '}';
    }
}
