package com.example.yegor.dmaanimations2;

import android.graphics.Rect;
import android.support.annotation.IntDef;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.example.yegor.dmaanimations2.utils.Utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class FloatingViewTouchListener implements View.OnTouchListener {

    private static final String TAG = FloatingViewTouchListener.class.getName();
    private static final int DEFAULT_JITTER = 10;
    private static final int LONG_CLICK_DELAY = 500;

    private static final int OUTSIDE_DRAGGABLE_ZONE = 0;
    private static final int INSIDE_DRAGGABLE_ZONE_TOOLBAR = 1;
    private static final int INSIDE_DRAGGABLE_ZONE_OVERFLOW = 2;

    private final WindowManager mWindowManager;

    private int initialX;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;
    private long timeStart;
    @DraggableZoneType
    private int mTouchZoneType;

    public FloatingViewTouchListener(WindowManager windowManager) {
        mWindowManager = windowManager;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) v.getLayoutParams();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                timeStart = System.currentTimeMillis();
                mTouchZoneType = isInsideDraggableZone(v, event);
                initialX = layoutParams.x;
                initialY = layoutParams.y;
                initialTouchX = event.getRawX();
                initialTouchY = event.getRawY();
                return true;
            case MotionEvent.ACTION_MOVE:
                if (mTouchZoneType == OUTSIDE_DRAGGABLE_ZONE) {
                    return true;
                }
                int newX = initialX + (int) (event.getRawX() - initialTouchX);
                int newY = initialY + (int) (event.getRawY() - initialTouchY);
                if (Math.abs(initialX - newX) > DEFAULT_JITTER || Math.abs(initialY - newY) > DEFAULT_JITTER) {
                    layoutParams.x = initialX + (int) (event.getRawX() - initialTouchX);
                    layoutParams.y = initialY + (int) (event.getRawY() - initialTouchY);
                    mWindowManager.updateViewLayout(v, layoutParams);
                }
                return true;
            case MotionEvent.ACTION_UP:
                if (mTouchZoneType != INSIDE_DRAGGABLE_ZONE_TOOLBAR || Math.abs(initialX - layoutParams.x) > DEFAULT_JITTER || Math.abs(initialY - layoutParams.y) > DEFAULT_JITTER) {
                    return true;
                }
                long timeEnd = System.currentTimeMillis();
                if (timeEnd - timeStart > LONG_CLICK_DELAY) {
                    v.performLongClick();
                } else {
                    v.performClick();
                }
                return true;
        }
        return false;
    }

    @DraggableZoneType
    private int isInsideDraggableZone(View v, MotionEvent event) {
        View draggableZone;
        View toolbarContainer = Utils.findViewById(v, R.id.toolbar_container);
        View overflowDraggableZone = Utils.findViewById(v, R.id.draggable_zone);
        if (toolbarContainer == null && overflowDraggableZone == null) {
            return OUTSIDE_DRAGGABLE_ZONE;
        }
        draggableZone = toolbarContainer != null ? toolbarContainer : overflowDraggableZone;
        Rect outRect = new Rect();
        draggableZone.getDrawingRect(outRect);
        Rect rect = new Rect((int) event.getRawX(), (int) event.getY(), (int) event.getX(), (int) event.getY());
        boolean inside = outRect.contains(rect);
        if (!inside) {
            return OUTSIDE_DRAGGABLE_ZONE;
        } else if (toolbarContainer != null) {
            return INSIDE_DRAGGABLE_ZONE_TOOLBAR;
        } else {
            return INSIDE_DRAGGABLE_ZONE_OVERFLOW;
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({OUTSIDE_DRAGGABLE_ZONE, INSIDE_DRAGGABLE_ZONE_TOOLBAR, INSIDE_DRAGGABLE_ZONE_OVERFLOW})
    @interface DraggableZoneType {
    }
}
