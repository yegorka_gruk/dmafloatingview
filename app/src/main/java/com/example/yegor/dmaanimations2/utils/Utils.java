package com.example.yegor.dmaanimations2.utils;

import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.SparseArray;
import android.view.View;

import com.example.yegor.dmaanimations2.R;

public class Utils {

    public static Drawable tintDrawable(Drawable drawable, @ColorInt int color) {
        drawable.setTint(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            DrawableCompat.setTint(drawable, color);
        } else {
            drawable.mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        }
        return drawable;
    }

    @SuppressWarnings("unchecked")
    public static <T extends View> T findViewById(View view, @IdRes int id) {
        SparseArray<View> tag = (SparseArray<View>) view.getTag(R.id.view_holder_tag);
        if (tag != null) {
            View view1 = tag.get(id);
            if (view1 == null) {
                view1 = view.findViewById(id);
                tag.append(id, view1);
            }
            return (T) view1;
        } else {
            tag = new SparseArray<>();
            T viewById = view.findViewById(id);
            tag.append(id, viewById);
            return viewById;
        }
    }

    @Nullable
    public static Bitmap getDrawingCacheCopy(View view) {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        Bitmap drawingCache = view.getDrawingCache(/*true*/);
        if (drawingCache != null) {
            Bitmap bitmap = Bitmap.createBitmap(drawingCache);
            view.setDrawingCacheEnabled(false);
            return bitmap;
        }
        return null;
    }
}
