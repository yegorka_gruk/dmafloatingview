package com.example.yegor.dmaanimations2.animation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.Transition;
import android.support.transition.TransitionValues;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;

import com.example.yegor.dmaanimations2.utils.Utils;

public class RevealTransition extends Transition {

    private static final String DRAWING_CACHE_COPY = "custom:background:drawing_cache_copy";

    private final int startX;
    private final int startY;
    private final float startRadius;
    private final float endRadius;
    @IdRes
    private final int containerId;
    private final boolean animateParent;

    public RevealTransition(int startX, int startY, float startRadius, float endRadius, @IdRes int containerId, boolean animateParent) {
        this.startX = startX;
        this.startY = startY;
        this.startRadius = startRadius;
        this.endRadius = endRadius;
        this.containerId = containerId;
        this.animateParent = animateParent;
    }

    @Override
    public void captureStartValues(@NonNull TransitionValues transitionValues) {
        Bitmap drawingCacheCopy = Utils.getDrawingCacheCopy(transitionValues.view);
        transitionValues.values.put(DRAWING_CACHE_COPY, drawingCacheCopy);
    }

    @Override
    public void captureEndValues(@NonNull TransitionValues transitionValues) {
    }

    @Nullable
    @Override
    public Animator createAnimator(@NonNull ViewGroup sceneRoot, @Nullable TransitionValues startValues, @Nullable TransitionValues endValues) {
        if (startValues != null && endValues != null && endValues.view.getId() == containerId) {
            endValues.view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(endValues.view, startX, startY, startRadius, endRadius);
            circularReveal.setDuration(getDuration());
            circularReveal.setInterpolator(new AccelerateInterpolator());
            circularReveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    View parent = animateParent ? (View) endValues.view.getParent() : endValues.view;
                    Bitmap drawingCacheCopy = (Bitmap) startValues.values.get(DRAWING_CACHE_COPY);
                    parent.setBackground(new BitmapDrawable(drawingCacheCopy));
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    View parent = (View) endValues.view.getParent();
                    parent.setBackground(null);
                    endValues.view.setLayerType(View.LAYER_TYPE_NONE, null);
                }
            });
            return circularReveal;
        }
        return null;
    }
}
