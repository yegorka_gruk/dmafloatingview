package com.example.yegor.dmaanimations2;

import android.app.Application;

public class App extends Application {

    private static App sApp;

    public static App getApp() {
        return sApp;
    }

    private PreferencesUtil mPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        mPreferences = new PreferencesUtil(this, BuildConfig.APPLICATION_ID);
        sApp = this;
    }

    public PreferencesUtil getPreferences() {
        return mPreferences;
    }
}
