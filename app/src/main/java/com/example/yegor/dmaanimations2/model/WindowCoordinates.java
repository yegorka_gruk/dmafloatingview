package com.example.yegor.dmaanimations2.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.text.TextUtils;

public class WindowCoordinates implements Parcelable {

    public static final WindowCoordinates DEFAULT = new WindowCoordinates(0, 100);

    private static final String DELIMITER = "\\|";

    public static final Parcelable.Creator<WindowCoordinates> CREATOR = new Parcelable.Creator<WindowCoordinates>() {
        @Override
        public WindowCoordinates createFromParcel(Parcel source) {
            return new WindowCoordinates(source);
        }

        @Override
        public WindowCoordinates[] newArray(int size) {
            return new WindowCoordinates[size];
        }
    };

    private int mX;
    private int mY;

    public WindowCoordinates() {
    }

    public WindowCoordinates(int x, int y) {
        this.mX = x;
        this.mY = y;
    }

    protected WindowCoordinates(Parcel in) {
        this.mX = in.readInt();
        this.mY = in.readInt();
    }

    public int getX() {
        return mX;
    }

    public void setX(int mX) {
        this.mX = mX;
    }

    public int getY() {
        return mY;
    }

    public void setY(int mY) {
        this.mY = mY;
    }

    public void setCoordinates(int[] coordinates) {
        this.mX = coordinates[0];
        this.mY = coordinates[1];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mX);
        dest.writeInt(this.mY);
    }

    @Override
    public String toString() {
        return mX + "|" + mY;
    }

    public static WindowCoordinates fromString(String s) {
        String[] strings = TextUtils.split(s, DELIMITER);
        if (strings.length != 2) {
            throw new IllegalStateException();
        }
        return new WindowCoordinates(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]));
    }


    public static WindowCoordinates fromIntArray(@NonNull @Size(2) int[] coordinates) {
        if (coordinates.length != 2) {
            throw new IllegalStateException();
        }
        return new WindowCoordinates(coordinates[0], coordinates[1]);
    }
}
