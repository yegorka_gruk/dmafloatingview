package com.example.yegor.dmaanimations2;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.example.yegor.dmaanimations2.model.AppBarTextModel;
import com.example.yegor.dmaanimations2.model.AppBarTextStyleModel;
import com.example.yegor.dmaanimations2.model.FloatingWindowType;
import com.example.yegor.dmaanimations2.model.WindowCoordinates;

public class MainActivity extends AppCompatActivity {

    /*  Permission request code to draw over other apps  */
    private static final int DRAW_OVER_OTHER_APP_PERMISSION_REQUEST_CODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DRAW_OVER_OTHER_APP_PERMISSION_REQUEST_CODE) {
            //Check if the permission is granted or not.
            if (resultCode == RESULT_OK)
                //If permission granted start floating widget service
                startFloatingWidgetService();
            else
                //Permission is not available then display toast
                Toast.makeText(this,
                        getResources().getString(R.string.draw_other_app_permission_denied),
                        Toast.LENGTH_SHORT).show();

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*  start floating widget service  */
    public void createFloatingWidget(View view) {
        //Check if the application has draw over other apps permission or not?
        //This permission is by default available for API<23. But for API > 23
        //you have to ask for the permission in runtime.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION_REQUEST_CODE);
        } else {
            //If permission is granted start floating widget service
            startFloatingWidgetService();
        }
    }

    /*  Start Floating widget service and finish current activity */
    private void startFloatingWidgetService() {
        Intent intent = new Intent(MainActivity.this, FloatingWidgetService.class);
        intent.putExtra(FloatingWidgetService.FLOATING_BUTTON_TYPE_EXTRA, FloatingWindowType.PRIVATE_CUSTOMER);
//        AppBarTextStyleModel[] expandedTitleText = {new AppBarTextStyleModel(R.color.floating_button_light_white, "1234", Gravity.START | Gravity.CENTER_VERTICAL), new AppBarTextStyleModel(R.color.floating_button_light_white, "56", Gravity.RIGHT | Gravity.CENTER_VERTICAL)};
//        AppBarTextModel value = new AppBarTextModel("Life is good!", android.R.color.holo_red_light, android.R.color.holo_green_light, expandedTitleText);
        AppBarTextStyleModel[] expandedTitleText = {new AppBarTextStyleModel(R.color.floating_button_customer, "Life is good!", Gravity.CENTER)};
        AppBarTextModel value = new AppBarTextModel("Life is good!", android.R.color.white, expandedTitleText);
        intent.putExtra(FloatingWidgetService.TOOLBAR_TITLE_EXTRA, value);
        String savedPositionStr = App.getApp().getPreferences().getFloatingWidgetSavedPosition();
        intent.putExtra(FloatingWidgetService.WINDOW_COORDINATES_EXTRA, savedPositionStr == null ? WindowCoordinates.DEFAULT : WindowCoordinates.fromString(savedPositionStr));
        startService(intent);
        finish();
    }

    public void stopFloatingWidget(View view) {
        stopService(new Intent(MainActivity.this, FloatingWidgetService.class));
        finish();
    }
}
