package com.example.yegor.dmaanimations2.model;

public class WheelPickerItemModel {

    private final String mTitle;
    private final int mMinutes;

    public WheelPickerItemModel(String title, int minutes) {
        this.mTitle = title;
        this.mMinutes = minutes;
    }

    public String getTitle() {
        return mTitle;
    }

    public int getMinutes() {
        return mMinutes;
    }

    public static WheelPickerItemModel create(String message) {
        String[] split = message.split("\\|");
        if (split.length != 2) {
            throw new IllegalArgumentException();
        }
        return new WheelPickerItemModel(split[0], Integer.parseInt(split[1]));
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
