package com.example.yegor.dmaanimations2.model;

public enum ButtonAction {
    RemindMe,
    CustomerProfile,
    BusinessProfile,
    Note,
    Sharing,
    ReportSpam,
    ReportUnSpam
}
